---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Strategy Analysis
The worst bias in this strategy in my opinion is the *survivorship bias*, since there is no way to find the index's companies at older times, I have no way to get over it, unless I put *all* the companies in the strategies, but computer time is scarce.

The portfolio balancing is done with
```{code-cell}
strategy_weights = [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ] 
```
which means you are buying one part of each of the 10 best forecasts (*from high to low*).

This means the portolio has a weight of 10, thus the comparison with the index is its' $return\ \times\ sum(weights)$. Also the *train/test split is .7/.3.*

## Data Setup
Due to *70* tickers being analyzed, I assumed that the return is **stationary**. It is uncalled for analyzing two or more *unit-root* tests over all companies but in a production setting it should be done more sistematically, and should be over watch everyweek.

**Before modelling** is important to look over at least the *(Partial) Autocorrelation plots*. The plots were all generated and looked over, but for a simple analysis let's look at an *overlay* of all the plots, first:

```{image} /images/ACF_overlay.png
:alt: AutoCorr_overlay_plot
:align: center
```

Obviously, there is no *mean* ACplot, but we can see that there is lots of significance at the *15th* lag and *21th*.

```{image} /images/PACF_overlay.png
:alt: PartialAutoCorr_overlay_plot
:align: center
```

Again I can see that there is lots of significance at the *15th* lag and *21th*.
Since It would be too cumbersome to fit different models, I decided to fit "an ok" model to everyone. The only way to do it is to look at *Information Criteria*. I created two plots with *AIC* for *MA* and *AR*:

```{image} /images/ar_ma_lags.png
:alt: AR_MA_Lags
:align: center
```

The limit for *AIC* calcularions is (4,2), since this is also computationally expensive. Looking at the *MA* graph, adding both lags (*1,2*) is proportionally less than the *0*. Since forecasting by hand *MA* lags is a bit troublesome, I accept *0*. The *AR* on the other way, there is proportionally more companies with lags than not, and I've chosen the *4* lags because if they are not statistically significant, their *coefficients tends to zero*. Adding the seasonal lag of *15* discussed before, I choose a (4,0,0)m(1,0,0,15) SARIMA model.

## Strategy Return
### In Training Sample
After running the models and using the forecast based on the weights before, I set an strategy for each forecast defining the buy at open and sell at close in the next day. Looking at the graph the strategy really outshines in the trainning sample.
```{image} /images/cum_daily_returns.png
:alt: Sample_return
:align: center
```

Looking at the first days of returns of *ibov* and the strategy, it is easy to see that the strategy has better *highs* and few lower *lows*.
```{image} /images/daily_return_train200.png
:alt: Daily_return
:align: center
```

### Out of Sample
But that is not the whole story, looking at *out-of-sample* things are not great. But the strategy still gives the investor a few days to jump out of the ship. Though after the *COVID* most buy-sell strategy, specially without *refitting* the models, things are not good for anyone.
```{image} /images/cumm_return_oos.png
:alt: OOS_return
:align: center
```

Imagine if I stopped at the beggining of the pandemic and picked-up just later, without refitting the model the result is poor.
```{image} /images/cumm_return_oos2.png
:alt: OOS_after_covid_return
:align: center
```

## Conclusion

Leaving out the *suvivorship bias*, I think the best take is that without *model reffiting*, or any kind of *continuous model performance evalution* no investment strategy will sail through all the seas.